﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using VersioningSerialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VersioningSerialization.Tests
{
    [TestClass()]
    public class PersonSerializerTests
    {
        private List<New.Person> NewPersons { get; set; }
        private List<Old.Person> OldPersons { get; set; }
        private Serializer PersonSerializer { get; set; }


        [TestInitialize]
        public void Setup()
        {
            PersonSerializer = new Serializer();
            NewPersons = new List<New.Person> {new New.Person() {Name = "John", Surname = "Doe"}, new New.Person() {Name = "Jane", Surname = "Doe"}};
            OldPersons = new List<Old.Person> {new Old.Person() {Name = "John"}, new Old.Person() {Name = "Jane"}};
        }

        [TestCleanup]
        public void Teardown()
        {
            PersonSerializer = null;
            NewPersons = null;
            OldPersons = null;
        }

        [TestMethod()]
        public void OldToOld()
        {
            var personXml = PersonSerializer.Serialize(OldPersons);
            var pep = PersonSerializer.DeSerialize<List<Old.Person>>(personXml);

            Assert.IsTrue(pep.Count == 2);
        }

        [TestMethod]
        public void OldToNew()
        {
            var personXml = PersonSerializer.Serialize(OldPersons);
            var pep = PersonSerializer.DeSerialize<List<New.Person>>(personXml);

            Assert.IsTrue(pep.Count == 2);
        }

        [TestMethod]
        public void NewToOld()
        {
            var personXml = PersonSerializer.Serialize(NewPersons);
            var pep = PersonSerializer.DeSerialize<List<Old.Person>>(personXml);

            Assert.IsTrue(pep.Count == 2);
        }

        [TestMethod]
        public void NewToNew()
        {
            var personXml = PersonSerializer.Serialize(NewPersons);
            var pep = PersonSerializer.DeSerialize<List<New.Person>>(personXml);

            Assert.IsTrue(pep.Count == 2);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace VersioningSerialization.Old
{
    public class Person
    {
        public string Name { get; set; }
    }
}

namespace VersioningSerialization.New
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}


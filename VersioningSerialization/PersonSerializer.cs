﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace VersioningSerialization
{
    public class Serializer
    {
        public string Serialize<T>(T persons)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                xsSubmit.Serialize(writer, persons);
                return sww.ToString();
            }
        }

        public T DeSerialize <T>(string objData)
        {
            var serializer = new XmlSerializer(typeof(T));
            object result;

            using (TextReader reader = new StringReader(objData))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

    }
}
